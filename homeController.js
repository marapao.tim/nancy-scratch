var app = angular.module('homeApp', ["ngStorage"]);  
app.controller('homeCtrl', function($scope, $localStorage, $sessionStorage, $http) {
 	 var sessionName = window.localStorage.getItem('username');
 	 if(sessionName == "" || sessionName == null){
 	 	//window.location.href = "login.html"; 
 	 }
 	 else{
 	 	$('#log').hide();
 	 	$('#sign').hide();
 	 } 
 	 $scope.Firstname = window.localStorage.getItem('username');

 	$scope.logOut = function(){ 
 		bootbox.confirm("Are you sure you want to log out?", function(result){ 
 			if(result){
				window.location.href = "login.html"; 
 				window.localStorage.setItem('username', ""); 
 			}
 		});
 		// window.location.href = "login.html"; 
 		// window.localStorage.setItem('username', "");
 	} 
}) 

