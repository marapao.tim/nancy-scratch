var app = angular.module('mainApp', []);  
app.controller('mainCtrl', function($scope, $http) {
	// $http.get('http://localhost:6969/student')
	// .then(function(response){ 
	// 	$scope.students = response.data;   
	// }); 
		$('#formField').hide();
		var studentID = 0;
		var add = true;
 		viewData();



	 	$scope.showForm = function(){
	 		$('#formField').show(1000);
	 		$('#tableStudent').hide(1000);  
	 		add = true; 
	 	}

	 	$scope.closeForm = function(){
	 		$('#formField').hide(1000);
	 		$('#tableStudent').show(1000);  
	 	}

       	$scope.saveStudent = function(){ 
	       	var firstName = $('#fname').val(); 
	       	var middleName = $('#mname').val();
	       	var lastName = $('#lname').val();
	       	var Age = $('#ageA').val();
	       	var gender = $("input[name=gender1]:checked").val();
	       	var address = $('#address').val(); 
	       	var stuID = parseInt($('#idfield').val())  
	       	if(add == true){
			    var request = $http({ 
		          method: "post",
		          url: "http://localhost:6969/insertStudent?firstname="+ firstName +"&middlename="+middleName+"&lastname="+lastName+"&AAge="+Age+"&gender="+gender+"&address="+address,
		        }); 

		        request.success(function (data){     
		        	viewData();  
		        	alert("Successfully Added!"); 
		        	$('#formField').hide(1000);
		 			$('#tableStudent').show(1000);
		 			_clear();
		      	}); 
	       	}  
	       	else{
		       	var request = $http({ 
		          method: "post",
		          url: "http://localhost:6969/updateStudent?firstname="+ firstName +"&middlename="+middleName+"&lastname="+lastName+"&AAge="+Age+"&gender="+gender+"&address="+address+"&id="+$scope.student.id,
		        }); 

		        request.success(function (data){         
		        	viewData();
		        	alert("Successfully Updated!"); 
		        	$('#formField').hide(1000);
		 			$('#tableStudent').show(1000); 
		 			_clear();
		      	});   
	       	}    
       	}; 

       	$scope.deleteData = function(studentID){
       		if(confirm("Are you sure you want to delete?") == true){
	       		var request = $http({ 
		          method: "post",
		          url: "http://localhost:6969/deleteStudent?id="+ studentID,
		        }); 

		        request.success(function (data){         
		        	viewData();
		        	alert("Successfully Delete!");
		        	 _clear();
		      	});  
       		} 
       	};

       	$scope.retrieve = function(student){  
	 		add = false;
			$scope.student = student;  
       		$('#formField').show(1000);
	 		$('#tableStudent').hide(1000);  
       	}

       	function viewData(){
		 var request = $http({ 
	            method: "GET",
	            url: 'http://localhost:6969/student',
	            headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
	          }); 
	      
	      request.success(function (data){         
	        $scope.students = data;
	      }); 
       }

       function _clear(){   
       	 //$('#frmStu').find('form')[0].reset();
       	 document.getElementById("frmStu").reset();
       	// $scope.student = " ";
       } 
}) 