var app = angular.module('animalApp', ["ngStorage"]);  
var img = "";
var img2 = "";


function loadFile(event) {
	var output = document.getElementById('output');
	output.src = URL.createObjectURL(event.target.files[0]);
	img = URL.createObjectURL(event.target.files[0]);
}

function base64(file, callback){
  var coolFile = {};
  function readerOnload(e){
    var base64 = btoa(e.target.result);
    coolFile.base64 = base64;
    callback(coolFile)
  };

  var reader = new FileReader();
  reader.onload = readerOnload;

  var file = file[0].files[0];
  coolFile.filetype = file.type;
  coolFile.size = file.size;
  coolFile.filename = file.name;
  reader.readAsBinaryString(file); 
  // var xhr = new XMLHttpRequest();
  // xhr.onload = function() {
  //   var reader = new FileReader();
  //   reader.onloadend = function() {
  //     callback(reader.result);
  //   }
  //   reader.readAsDataURL(xhr.response);
  // };
  // xhr.open('GET', url);
  // xhr.responseType = 'blob';
  // xhr.send();
}
 
app.controller('animalCtrl', function($scope, $http, $localStorage) { 


	viewData();
		
	$scope.SaveAnimal = function(){ 
    var name = $("#name").val();
    var type = $("#type").val();
    var description = $("#description").val();
    var lifespan = $("#lifespan").val();
    var livingtype = $("#livingtype").val(); 
    
	base64( $('input[type="file"]'), function(data){ 
    // var asdasd = "data:image/jpeg;base64," + data.base64;
    // console.log(asdasd.length);
   $scope.AnimalCredentials = {
    name: name,
    type: type, 
    description: description, 
    lifespan: lifespan, 
    livingtype: livingtype,
    image: "data:image/jpeg;base64," + data.base64
   }

    var request = $http({ 
       method: "post",
       url: "http://localhost:6969/insertAnimal?", 
       data: $.param($scope.AnimalCredentials),
       headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
    });   
    request.success(function (data){ 
      //alert("Success Added");
      $('#Alert').html("<div class='alert alert-success' role='alert'><a href='#' class='close' data-dismiss='alert'>&times;</a><strong>Congratulations! </strong>Successfully Added</div>");
       document.getElementById("frmAnimal").reset();
       document.getElementById("output").src="";
    });
	});   
	}

  $scope.view = function(name, type, description, lifespan, livingtype, image){
    //alert(name + type + description + lifespan + livingtype);
    $scope.name2 = name;
    $scope.type2 = type;
    $scope.description2 = description;
    $scope.lifespan2 = lifespan;
    $scope.livingtype2 = livingtype;
    $scope.image2 = image;
  }

  var IDS = 0;
  $scope.viewSelectData = function(id,animalName, typE, desciptioN, lifespaN, livingtypE){
    IDS = id;
    $("#namE").val(animalName);
    $("#typE").val(typE);
    $("#descriptioN").val(desciptioN);
    $("#lifespaN").val(lifespaN);
    $("#livingtypE").val(livingtypE); 
  }


  $scope.saveData = function(){ 
    var name = $("#namE").val();
    var type = $("#typE").val();
    var description = $("#descriptioN").val();
    var lifespan = $("#lifespaN").val();
    var livingtype = $("#livingtypE").val(); 

    $scope.AnimalCredentials2 = {
    id: IDS,
    name: name,
    type: type, 
    description: description, 
    lifespan: lifespan, 
    livingtype: livingtype
   }

   var request = $http({ 
       method: "post",
       url: "http://localhost:6969/updateAnimal?", 
       data: $.param($scope.AnimalCredentials2),
       headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
    });

    alert("succesfully Updated");
  }

  $scope.deleteAnimals = function(id){ 
    //if(confirm("Are you sure you want to delete?") == true){ 

    bootbox.confirm("Are you sure you want to delete?", function(result){ 
      if(result){
        var request = $http({ 
          method: "post",
          url: "http://localhost:6969/deleteAnimal?id=" + id,
        });

        request.success(function (data){         
        viewData();
        alert("Successfully Delete!"); 
        });  
      }
    }); 
    //} 
  }

	function viewData(){
		 var request = $http({ 
	            method: "GET",
	            url: 'http://localhost:6969/viewAnimal',
	            headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
	          }); 
	      
	      request.success(function (data){         
	        $scope.animals = data;
	      });  
		}  
})
