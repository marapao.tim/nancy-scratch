var app = angular.module('userApp', ["ngStorage", "ngCookies"]);  
app.controller('userCtrl', function($scope, $localStorage, $sessionStorage, $http, $cookies) {

	var USERNAME22 = $cookies.get('UsernameCookie');
	var PASSWORD22 = $cookies.get('PasswordCookie');   

	$scope.username123 = USERNAME22;
	$scope.password123 = PASSWORD22;
	$('#remember').prop('checked', true);
	if($scope.username123 == "" || $scope.username123 == null){
		$('#remember').prop('checked', false);
	} 
	var sessionName = window.localStorage.getItem('username');

	if(sessionName != ""){
		//location.href = "home.html"; 
	}
	else
	{

	}

	 $scope.logUser = function(){

	 	var request = $http({ 
		    method: "get",
		    url: "http://localhost:6969/user?username=" + $("#username").val() + "&password=" +$("#password").val(),
		}); 
		request.success(function (data){       
		    $scope.status = data.replace(/"/g,''); 
		   	if($scope.status != "Invalid Username or Password")
		    { 
		    	window.localStorage.setItem('username', $scope.status); 
		    	if(document.getElementById('remember').checked) 
		    	{
			    	$cookies.put('UsernameCookie', $('#username').val());
			    	$cookies.put('PasswordCookie', $('#password').val());
				} 
				else 
				{
				    $cookies.put('UsernameCookie', "");
			    	$cookies.put('PasswordCookie', "");
				}
		    	$scope.status = ""; 
				window.location.href = "home.html"; 
		    } 
		    else
		    {
		    	$('#errAlert').html("<div class='alert alert-danger' role='alert'><a href='#' class='close' data-dismiss='alert'>&times;</a><strong>Error! </strong>"+ $scope.status +"</div>");
		    }
		});
	 }
 
	 $scope.insertUser = function(){ 
	 	var firstname = $("#fname").val();
	 	var middlename = $("#mdname").val();
	 	var lastname = $("#lname").val();
	 	var age = $("#age").val();
	 	var gender = $("input[name=optradio]:checked").val();
	 	var emaillAdd = $("#emailAdd").val();
	 	var username = $("#username").val();
	 	var password = $("#password").val();
	 	var request = $http({ 
		   method: "post",
		   url: "http://localhost:6969/Insertuser?firstname="+firstname+"&middlename="+middlename+"&lastname="+lastname+"&age="+age+"&gender="+gender+"&emailAddress="+emaillAdd+"&username="+username+"&password="+password,
		}); 
		request.success(function (data){ 
			var RegStatus = data.replace(/"/g,'');
			var AlertStatus = "danger";
			var ErrCong = "Error";
			if(RegStatus == "Successfully Registered")
			{
				document.getElementById("frmSign").reset();
				AlertStatus = "success";
				ErrCong = "Congratulations";
			}       
			$('#successAlert').html("<div class='alert alert-"+ AlertStatus +"' role='alert'><a href='#' class='close' data-dismiss='alert'>&times;</a><strong>"+ ErrCong +"! </strong>"+ RegStatus +"</div>");
		}); 
	 }
}) 