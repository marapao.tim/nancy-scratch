﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nancy;
using Nancy.Hosting.Self;
using Mono.Unix.Native;
using Mono.Unix;

namespace ConsoleApplication3
{
    class Program
    {
        static void Main(string[] args)
        {
            var uri = "http://localhost:6969"; 
            Console.WriteLine("Rest Server starting run in " + uri); 

            var host = new NancyHost(new Uri(uri));
            host.Start();  
             
            if (Type.GetType("Mono.Runtime") != null)
            { 
                UnixSignal.WaitAny(new[] {
                    new UnixSignal(Signum.SIGINT),
                    new UnixSignal(Signum.SIGTERM),
                    new UnixSignal(Signum.SIGQUIT),
                    new UnixSignal(Signum.SIGHUP)
                });
            }
            else
            {
                Console.ReadLine();
            }

            Console.WriteLine("Stopping Rest Server");
            host.Stop();  
        }
    }
}
