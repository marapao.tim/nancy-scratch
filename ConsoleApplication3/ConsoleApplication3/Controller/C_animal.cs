﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using ConsoleApplication3.Model;

namespace ConsoleApplication3.Controller
{
    public class C_animal : M_animal
    {
        static C_connection connector = new C_connection();
        MySqlConnection connection = new MySqlConnection();
        MySqlDataReader reader;

        List<M_animal> listAnimal = new List<M_animal>();
        public string insertAnimal()
        {
            try
            {
                connection = connector.con();
                connection.Open();
                MySqlCommand comm = connection.CreateCommand();
                comm.CommandText = @"SET GLOBAL max_allowed_packet=1073741824;INSERT INTO `nancy`.`animal`
                ( 
                    `name`,
                    `type`,
                    `description`,
                    `lifespan`,
                    `living_type`,
                    `image`
                )
                VALUES
                ( 
                    @name,
                    @type,
                    @description,
                    @lifespan,
                    @living_type,
                    @image
                );";
                comm.Parameters.AddWithValue("@name", name);
                comm.Parameters.AddWithValue("@type", type);
                comm.Parameters.AddWithValue("@description", description);
                comm.Parameters.AddWithValue("@lifespan", lifespan);
                comm.Parameters.AddWithValue("@living_type", livingtype);
                comm.Parameters.AddWithValue("@image", image); 
                comm.ExecuteNonQuery();
                connection.Close();
                return true.ToString();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error Insert Animal: " + ex.ToString());
                return false.ToString();
            }
        }


        public string updateAnimal(int ID)
        {
            try
            {
                connection = connector.con();
                connection.Open();
                MySqlCommand comm = connection.CreateCommand();
                comm.CommandText = @"SET GLOBAL max_allowed_packet=1073741824;UPDATE `nancy`.`animal`
                                SET 
                                `name` = @name,
                                `type` = @type,
                                `description` = @description,
                                `lifespan` = @lifespan,
                                `living_type` = @living_type 
                                WHERE `id` = @id;
                                ";
                comm.Parameters.AddWithValue("@id", ID);
                comm.Parameters.AddWithValue("@name", name);
                comm.Parameters.AddWithValue("@type", type);
                comm.Parameters.AddWithValue("@description", description);
                comm.Parameters.AddWithValue("@lifespan", lifespan);
                comm.Parameters.AddWithValue("@living_type", livingtype); 
                comm.ExecuteNonQuery();
                connection.Close();
                return true.ToString();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error Insert Animal: " + ex.ToString());
                return false.ToString();
            }
        }


        public string deleteAnimal(int AnimalID)
        {
            try
            {
                connection = connector.con();
                connection.Open();
                MySqlCommand comm = connection.CreateCommand();
                comm.CommandText = @"DELETE FROM `nancy`.`animal` WHERE id = @id;";
                comm.Parameters.AddWithValue("@id", AnimalID);  
                comm.ExecuteNonQuery();
                connection.Close();
                return true.ToString();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error Delete Animal: " + ex.ToString());
                return false.ToString();
            }
        }


        public List<M_animal> listAnimal2()
        {
            try
            {
                connection = connector.con();
                connection.Open();
                MySqlCommand cmd = new MySqlCommand("SELECT * FROM nancy.animal;", connection);
                reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    listAnimal.Add(new M_animal
                    {
                        id = Convert.ToInt32(reader.GetString("id")),
                        name = reader.GetString("name").ToString(),
                        type = reader.GetString("type").ToString(),
                        description = reader.GetString("description").ToString(),
                        lifespan = reader.GetString("lifespan").ToString(),
                        livingtype = reader.GetString("living_type").ToString(),
                        image = reader.GetString("image").ToString(),
                    });
                }

                reader.Close();
                connection.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error Getting Animal List: " + ex.ToString());
            }
            return listAnimal;
        }
    }
}
