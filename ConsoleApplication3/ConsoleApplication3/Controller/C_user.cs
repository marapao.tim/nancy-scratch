﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConsoleApplication3.Model;
using MySql.Data.MySqlClient;

namespace ConsoleApplication3.Controller
{
    public class C_user : M_user
    { 
        MySqlConnection connection = new MySqlConnection();
        MySqlDataReader reader;
        C_connection connector = new C_connection();

        public string login()
        {
            string status = "";
            try
            {
                connection = connector.con();
                connection.Open();
                MySqlCommand cmd = new MySqlCommand("SELECT * FROM nancy.user where username = @user and password = @pass;", connection);
                cmd.Parameters.AddWithValue("@user", username);
                cmd.Parameters.AddWithValue("@pass", password);
                reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                    status = reader.GetString("first_name").ToString();
                }
                else
                {
                    status = "Invalid Username or Password";
                }

                reader.Close();
                connection.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error Getting Student List: " + ex.ToString());
            } 
            return status;
        }

        public string insertUser()
        {
            try
            {
                connection = connector.con();
                connection.Open();
                if(existUsername(username))
                {
                    return "Username is already Exist";
                }
                else
                {
                    MySqlCommand comm = connection.CreateCommand();
                    comm.CommandText = @"INSERT INTO `nancy`.`user`
                (
                    `first_name`,
                    `middle_name`,
                    `last_name`,
                    `age`,
                    `gender`,
                    `email_address`,
                    `username`,
                    `password`
                )
                VALUES
                (
                    @first_name,
                    @middle_name,
                    @last_name,
                    @age,
                    @gender,
                    @email_address,
                    @username,
                    @password
                );

                    ";
                    comm.Parameters.AddWithValue("@first_name", firstname);
                    comm.Parameters.AddWithValue("@middle_name", middlename);
                    comm.Parameters.AddWithValue("@last_name", lastname);
                    comm.Parameters.AddWithValue("@age", age);
                    comm.Parameters.AddWithValue("@gender", gender);
                    comm.Parameters.AddWithValue("@email_address", emailAddress);
                    comm.Parameters.AddWithValue("@username", username);
                    comm.Parameters.AddWithValue("@password", password);
                    comm.ExecuteNonQuery();
                    connection.Close();
                    return "Successfully Registered"; 
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error Insert User: " + ex.ToString());
                return false.ToString();
            }
        }

        bool existUsername(string uname)
        {
            bool exist = false;
            MySqlCommand cmd = new MySqlCommand("SELECT * FROM nancy.user where username = @user;", connection);
            cmd.Parameters.AddWithValue("@user", uname); 
            reader = cmd.ExecuteReader();
            if (reader.Read())
            {
                exist = true;
            }
            else
            {
                exist = false;
            } 
            reader.Close(); 
            return exist;
        }
    }
}
