﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConsoleApplication3.Model;
using MySql.Data.MySqlClient;

namespace ConsoleApplication3.Controller
{
    public class C_student : M_student
    {

        MySqlConnection connection = new MySqlConnection();
        MySqlDataReader reader;
        C_connection connector = new C_connection();

       // M_student mStu = new M_student();
        List<M_student> listStu = new List<M_student>();

        public List<M_student> arrayStu()
        {
            try
            {
                connection = connector.con();
                connection.Open();
                MySqlCommand cmd = new MySqlCommand("SELECT * FROM nancy.student;", connection);
                reader = cmd.ExecuteReader();
                while (reader.Read())
                {  
                    listStu.Add(new M_student
                    {
                        id = Convert.ToInt32(reader.GetString("id")),
                        firstname = reader.GetString("first_name").ToString(),
                        middlename = reader.GetString("middle_name").ToString(),
                        lastname = reader.GetString("last_name").ToString(),
                        AAge = Convert.ToInt32(reader.GetString("age")),
                        gender = reader.GetString("gender").ToString(),
                        address = reader.GetString("address").ToString(), 
                    });
                } 
            
                reader.Close();
                connection.Close();
            }catch(Exception ex)
            {
                Console.WriteLine("Error Getting Student List: " + ex.ToString());
            }
            return listStu;
        }

        public string insertStudent()
        { 
            try
            {
                connection = connector.con();
                connection.Open();
                MySqlCommand comm = connection.CreateCommand(); 
                        comm.CommandText = @"INSERT INTO `nancy`.`student`
                    (
                        `first_name`,
                        `middle_name`,
                        `last_name`,
                        `age`,
                        `gender`,
                        `address`
                    )
                    VALUES
                    (
                        @first_name,
                        @middle_name,
                        @last_name,
                        @age,
                        @gender,
                        @address
                    );
                    ";  
                comm.Parameters.AddWithValue("@first_name", firstname);
                comm.Parameters.AddWithValue("@middle_name", middlename);
                comm.Parameters.AddWithValue("@last_name", lastname);
                comm.Parameters.AddWithValue("@age", AAge);
                comm.Parameters.AddWithValue("@gender", gender);
                comm.Parameters.AddWithValue("@address", address);
                comm.ExecuteNonQuery();
                connection.Close();
                return true.ToString();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error Insert Student: " + ex.ToString());
                return false.ToString();
            }
        }


        public string updateStudent(string studentID)
        {
            try
            {
                connection = connector.con();
                connection.Open();
                MySqlCommand comm = connection.CreateCommand(); 
                    comm.CommandText = @"UPDATE `nancy`.`student`
                    SET 
                    `first_name` = @first_name,
                    `middle_name` = @middle_name,
                    `last_name` = @last_name,
                    `age` = @age,
                    `gender` = @gender,
                    `address` = @address
                    WHERE `id` = '" + studentID + @"';
                    "; 
                comm.Parameters.AddWithValue("@first_name", firstname);
                comm.Parameters.AddWithValue("@middle_name", middlename);
                comm.Parameters.AddWithValue("@last_name", lastname);
                comm.Parameters.AddWithValue("@age", AAge);
                comm.Parameters.AddWithValue("@gender", gender);
                comm.Parameters.AddWithValue("@address", address);
                comm.ExecuteNonQuery();
                connection.Close();
                return true.ToString();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error Update Student: " + ex.ToString());
                return false.ToString();
            }
        }

        public string deleteStudent(int studentID)
        {
            try
            {
                connection = connector.con();
                connection.Open();
                MySqlCommand comm = connection.CreateCommand();
                comm.CommandText = @"DELETE FROM `nancy`.`student` WHERE id = '"+ studentID +"'"; 
                comm.ExecuteNonQuery();
                connection.Close();
                return true.ToString();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error Delete Student: " + ex.ToString());
                return false.ToString();
            }
        }
    }
}
