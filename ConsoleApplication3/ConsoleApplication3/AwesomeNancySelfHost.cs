﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nancy;

namespace ConsoleApplication3
{ 
   public class AwesomeNancySelfHost : NancyModule
   {
      public AwesomeNancySelfHost()
      { 
         Get["/v1/feeds"] = parameters =>
         {
             var feeds = new string[] { "foo", "bar" }; 
             return Response.AsJson(feeds);
         }; 
      }
   } 
}
