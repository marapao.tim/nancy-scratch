﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication3.Model
{
    public class M_user
    {
        public string firstname { get; set; }
        public string middlename { get; set; }
        public string lastname { get; set; }
        public string age { get; set; }
        public string gender { get; set; }
        public string emailAddress { get; set; }
        public string username { get; set; }
        public string password { get; set; }
    }
}
