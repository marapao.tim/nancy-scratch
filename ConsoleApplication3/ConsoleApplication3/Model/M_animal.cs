﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication3.Model
{
    public class M_animal
    {
        public int id { get; set; }
        public string name { get; set; }
        public string type { get; set; }
        public string description { get; set; }
        public string lifespan { get; set; }
        public string livingtype { get; set; }
        public string image { get; set; }
    }
}
