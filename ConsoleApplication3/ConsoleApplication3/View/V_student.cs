﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nancy;
using Newtonsoft.Json;
using ConsoleApplication3.Controller;
using Nancy.ModelBinding;

namespace ConsoleApplication3.View
{
    public class V_student : NancyModule
    {
        public V_student()
        {
            Get["/student"] = param =>
            {
                C_student cStu = new C_student();
                return JsonConvert.SerializeObject(cStu.arrayStu());
            };
            Post["/insertStudent{parameters}"] = param => { 
                C_student cStu = new C_student();
                cStu = this.Bind<C_student>(); 
                return JsonConvert.SerializeObject(cStu.insertStudent());
            };

            Post["/updateStudent{parameters}"] = param => {
                string id = this.Request.Query["id"];
                C_student cStu = new C_student();
                cStu = this.Bind<C_student>();
                return JsonConvert.SerializeObject(cStu.updateStudent(id));
            };

            Post["/deleteStudent{parameters}"] = param => {
                int id = this.Request.Query["id"];
                C_student cStu = new C_student();
                cStu = this.Bind<C_student>();
                return JsonConvert.SerializeObject(cStu.deleteStudent(id));
            };
        }
    }
}
