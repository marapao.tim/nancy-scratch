﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nancy;
using Nancy.Bootstrapper;
using Nancy.TinyIoc;

namespace ConsoleApplication3.View
{
    public class CORS
    {
        public class Bootstrapper : DefaultNancyBootstrapper
        {
            protected override void RequestStartup(TinyIoCContainer container, IPipelines pipelines, NancyContext context)
            {
                pipelines.AfterRequest.AddItemToEndOfPipeline((ctx) =>
                {
                    ctx.Response.WithHeader("Access-Control-Allow-Origin", "*")
                                    .WithHeader("Access-Control-Allow-Methods", "POST,GET")
                                    .WithHeader("Access-Control-Allow-Headers", "Accept, Origin, Content-type");

                });
            }
        }
    }
}
