﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks; 
using Nancy;
using Newtonsoft.Json;
using ConsoleApplication3.Controller;
using Nancy.ModelBinding;

namespace ConsoleApplication3.View
{
    public class V_user : NancyModule
    {
        public V_user(){
        Get["/user{parameter}"] = param =>
            {
                C_user cUser = new C_user();
                cUser = this.Bind<C_user>();
                return JsonConvert.SerializeObject(cUser.login());
            };
         Post["/Insertuser{parameter}"] = param =>
            {
                C_user cUser = new C_user();
                cUser = this.Bind<C_user>();
                return JsonConvert.SerializeObject(cUser.insertUser());
            };
        }

    }
}
