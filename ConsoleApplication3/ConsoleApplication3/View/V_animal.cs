﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConsoleApplication3.Controller;
using Nancy;
using Newtonsoft.Json;
using Nancy.ModelBinding;

namespace ConsoleApplication3.View
{
    public class V_animal : NancyModule
    {
        public V_animal()
        {
            Get["/viewAnimal{parameters}"] = param => {
                C_animal cAnimal = new C_animal(); 
                return JsonConvert.SerializeObject(cAnimal.listAnimal2());
            };

            Post["/insertAnimal{parameters}"] = param => {
                C_animal cAnimal = new C_animal();
                cAnimal = this.Bind<C_animal>();
                return JsonConvert.SerializeObject(cAnimal.insertAnimal());
            };

            Post["/updateAnimal{parameters}"] = param =>
            {
                int id = this.Request.Query["id"];
                C_animal cAnimal = new C_animal();
                cAnimal = this.Bind<C_animal>();
                return JsonConvert.SerializeObject(cAnimal.updateAnimal(id));
            };

            Post["/deleteAnimal{parameters}"] = param => {
                C_animal cAnimal = new C_animal();
                int id = this.Request.Query["id"];
                cAnimal = this.Bind<C_animal>();
                return JsonConvert.SerializeObject(cAnimal.deleteAnimal(id));
            };
        }
    }
}
